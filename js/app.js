const form = document.getElementById('formConversor');
const submit = document.getElementById('calcular');
const error = document.getElementById('error');
const euros = document.getElementById('euros');
const dolares = document.getElementById('dolares');
const yenes = document.getElementById('yenes');
const pesos = document.getElementById('pesos');

const valorDolar = 1.05;
const valorYenes = 140.97;
const valorPesos = 58.99;

function mostarResultados() {

    dolares.style.display = "block";
    yenes.style.display = "block";
    pesos.style.display = "block";

}

function ocultarResultados(){
    dolares.style.display = "none";
    yenes.style.display = "none";
    pesos.style.display = "none";
}

function calcular(event) {

    event.preventDefault();
    error.style.display='block';

    if (euros.value.length != 0 && !isNaN(euros.value) ) {
        let valorEuros = parseInt(euros.value, 10);

        if(valorEuros>=0){
            error.style.display='none';
            mostarResultados();
            dolares.innerHTML = "Dolares:\t\t" + (valorEuros * valorDolar).toFixed(2) + " $"
            yenes.innerHTML = "Yenes:\t\t" + (valorEuros * valorYenes).toFixed(2) + " \u00A5"
            pesos.innerHTML = "Pesos:\t\t" + (valorEuros * valorPesos).toFixed(2) + " \u20B1"
        
        }else{
            ocultarResultados();
            error.innerHTML="El numero debe ser mayor que 0"
        }
    } else  {
        ocultarResultados();
        error.innerHTML = "Debe indicar un numero";
    }
}



form.addEventListener('submit', calcular);